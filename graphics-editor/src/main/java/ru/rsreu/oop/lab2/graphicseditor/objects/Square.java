package ru.rsreu.oop.lab2.graphicseditor.objects;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Square {

    private Figure figure;

    public Square(double x, double y, double size, Color color) {
        figure = new Figure(x, y, size, color);
    }


    public void draw(Canvas canvas) {
        figure.draw(canvas).strokeRect(figure.getX(),figure.getY(),figure.getSize(),figure.getSize());
    }

}
