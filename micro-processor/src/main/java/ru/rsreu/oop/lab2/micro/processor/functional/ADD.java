package ru.rsreu.oop.lab2.micro.processor.functional;

public class ADD {

    private Command command;

    public ADD(short reg1, short reg2){
       command = new Command(reg1, reg2);
    }

    public short executeCommand() {
        return (short) (command.getRegister1() + command.getRegister2());
    }

}
