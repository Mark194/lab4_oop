package ru.rsreu.oop.lab2.micro.processor.view;

import javafx.scene.control.TextField;

public class SpecTextField extends TextField {

    private static final String HEX = "1234567890abcdef.\b";
    private static final String BIN = "01\b";
    private String notation;

    public SpecTextField(String string) {
        super(string);
    }

    @Override
   public void replaceText(int start, int end, String text) {
        if (!getLineOfNotation().contains(text))
            text = "";
        else {
            if (!this.getText().contains("0x") && notation == "16")
                text = "0x"+this.getText()+text;
        }
        super.replaceText(start, end, text);
    }

    public String getLineOfNotation(){
        switch (notation){
            case "16": {
                return HEX;
            }
            case "2": {
                return BIN;
            }
            default: {
                System.out.println("Неизвестная система счисления");
                return null;
            }
        }
    }

    public String getNotation() {
        return notation;
    }

    public void setNotation(String notation) {
        this.notation = notation;
    }
}
