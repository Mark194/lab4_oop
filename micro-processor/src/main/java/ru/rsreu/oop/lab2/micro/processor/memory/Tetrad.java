package ru.rsreu.oop.lab2.micro.processor.memory;

public class Tetrad extends Data {
    private char value;

    public Tetrad(char symbol){
        this.value = symbol;
    }

    public char getValue() {
        return value;
    }
}
