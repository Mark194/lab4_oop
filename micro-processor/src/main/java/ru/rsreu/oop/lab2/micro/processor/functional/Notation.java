package ru.rsreu.oop.lab2.micro.processor.functional;

import javafx.scene.control.ChoiceBox;

public class Notation {

    public int countUnitsInBinaryNumber(String number){
        int count = 0;
        for (char symbol : number.toCharArray()) {
            if (symbol == '1')
                count++;
        }
        return count;
    }

    public String convertHexToBinary(String number){
        int num = Integer.parseUnsignedInt(number, 16);
        return Integer.toBinaryString(num);
    }

    public String convertBinaryToHex(String number){
        int num = Integer.parseUnsignedInt(number, 2);
        return Integer.toHexString(num);
    }

    public String getNotation(int index){
        switch (index){
            case 0: {
                return "16";
            }
            case 1: {
                return "2";
            }
            default: {
                System.out.println("����������� ������� ���������");
                return null;
            }
        }
    }

    public String delSymbolsInHex(String line){
        StringBuilder newLine = new StringBuilder(line);
        if (newLine.indexOf("0x") == 0){
            newLine = newLine.deleteCharAt(0).deleteCharAt(0);
        }
        return newLine.toString();
    }

    public String addSymbolsInHex(String line){
        StringBuilder newLine = new StringBuilder("0x");
        if (!line.contains("0x")){
            newLine = newLine.append(line);
        }
        return newLine.toString();
    }

}
