package ru.rsreu.oop.lab2.micro.processor.paradigm;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Model extends Application {


    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(Controller.class.getClassLoader().getResource("fxml/model.fxml"));
        Parent parent = loader.load();
        final Controller controller = loader.getController();
        primaryStage.getIcons().add(new Image(
                Model.class.getClassLoader().getResource("image/processor.png").toString()));
        primaryStage.setOnShown(e -> {
            controller.loadElement();
        });
        primaryStage.setTitle("Микропроцессор");
        primaryStage.setScene(new Scene(parent, 600, 400));
        primaryStage.show();
    }
}
