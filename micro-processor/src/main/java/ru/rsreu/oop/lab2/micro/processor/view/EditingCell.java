package ru.rsreu.oop.lab2.micro.processor.view;

import javafx.scene.control.TableCell;
import ru.rsreu.oop.lab2.micro.processor.functional.Notation;

public class EditingCell extends TableCell<View, String> {

    private SpecTextField textField;
    private String notation;

    public EditingCell(int index){
        Notation notation = new Notation();
        this.notation = notation.getNotation(index);
    }

    public void changeNotation(String notation){
        this.notation = notation;
    }

    @Override
    public void startEdit(){
        if (!isEmpty()) {
            super.startEdit();
            createTextField();
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }
    }

    private void createTextField() {
        textField = new SpecTextField(getString());
        textField.setNotation(notation);
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue)
                commitEdit(textField.getText());
        });
    }


    @Override
    public void cancelEdit(){
        super.cancelEdit();
        setText((String) getItem());
        setGraphic(null);
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if(empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null)
                    textField.setText(getString());
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }
    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }

}
