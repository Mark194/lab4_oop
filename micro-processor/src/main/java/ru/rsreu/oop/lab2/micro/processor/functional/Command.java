package ru.rsreu.oop.lab2.micro.processor.functional;

public class Command {

    private short register1, register2;

    public Command(short reg1, short reg2){
        this.register1 = reg1;
        this.register2 = reg2;
    }

    public short getRegister1() {
        return register1;
    }

    public short getRegister2() {
        return register2;
    }

}
