package ru.rsreu.oop.lab2.micro.processor.memory;

import ru.rsreu.oop.lab2.micro.processor.functional.Notation;

import java.util.ArrayList;
import java.util.List;

public class Register {
    private List<Data> bits;
    private String notation;

    public Register(String line, String notation){
        bits = new ArrayList<>();
        this.notation = notation;
        this.setBits(line);
    }

    public List getBits() {
        return bits;
    }

    public List setBits(String number) {
        for (char symbol : number.toCharArray()) {
            switch (notation) {
                case "2": {
                    bits.add(new Bit(Character.getNumericValue(symbol)));
                    break;
                }
                case "16": {
                    bits.add(new Tetrad(symbol));
                    break;
                }
                default: {
                    System.out.println("Неизвестная система счисления");
                    break;
                }
            }
        }
        return bits;
    }

    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        for (Data bit : bits) {
            switch (bit.getClass().getSimpleName()) {
                case "Bit": {
                    stringBuilder.append(((Bit)(bit)).getValue());
                    break;
                }
                case "Tetrad": {
                    stringBuilder.append(((Tetrad)bit).getValue());
                    break;
                }
                default:{
                    stringBuilder = null;
                    break;
                }
            }
        }
        return stringBuilder.toString();
    }

    public void changeNotation(String notation){
        Notation not = new Notation();
        String line = this.toString();
        if (this.notation == "16" & notation == "2"){
            line = not.delSymbolsInHex(line);
            line = not.convertHexToBinary(line);
        } else {
            line = not.convertBinaryToHex(line);
            line = not.addSymbolsInHex(line);
        }
        this.notation = notation;
        this.bits.clear();
        this.setBits(line);
    }

}
