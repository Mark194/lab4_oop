package ru.rsreu.oop.lab2.micro.processor.paradigm;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import ru.rsreu.oop.lab2.micro.processor.functional.*;
import ru.rsreu.oop.lab2.micro.processor.view.EditingCell;
import ru.rsreu.oop.lab2.micro.processor.memory.Status;
import ru.rsreu.oop.lab2.micro.processor.view.View;

import java.util.*;

public class Controller {

    private static final int COUNT_REGISTERS = 16;
    private static final String[] COMMANDS = {"ADD", "SUB", "AND", "OR", "XOR"};
    private static final String[] NOTATIONS = {"Шестнадцатиричная", "Двоичная"};

    @FXML
    private ChoiceBox commandChoice;
    @FXML
    private ChoiceBox firstOperand;
    @FXML
    private ChoiceBox secondOperand;
    @FXML
    private ChoiceBox changeNotation;
    @FXML
    public TableView<View> registers;
    @FXML
    private TableView<Status>  regStatus;
    @FXML
    private TableColumn regName;
    @FXML
    private TableColumn regValue;
    @FXML
    private TableColumn flagN;
    @FXML
    private TableColumn flagP;
    @FXML
    private TableColumn flagV;
    @FXML
    private TableColumn flagZ;

    private ObservableList<View> regViews = FXCollections.observableArrayList();
    private Status status = new Status(0,0,0,0);
    private final ObservableList<String> register = createOperand();
    private Notation notation = new Notation();
    private EditingCell editingCell;


    public void loadElement(){
        commandChoice.setItems(FXCollections.observableList(Arrays.asList(COMMANDS)));
        changeNotation.setItems(FXCollections.observableList(Arrays.asList(NOTATIONS)));
        commandChoice.setValue(COMMANDS[0]);
        changeNotation.setValue(NOTATIONS[0]);
        firstOperand.setItems(register);
        firstOperand.setValue(register.get(0));
        secondOperand.setItems(register);
        secondOperand.setValue(register.get(1));
        initRegisters();
        Callback<TableColumn, TableCell> cellFactory = p -> {
            editingCell = new EditingCell(changeNotation.getSelectionModel().getSelectedIndex());
            return editingCell;
        };
        regName.setCellValueFactory(new PropertyValueFactory<View, String>("name"));
        regValue.setCellValueFactory(new PropertyValueFactory<View, String>("value"));
        regValue.setCellFactory(cellFactory);
        regValue.setOnEditCommit((EventHandler <TableColumn.CellEditEvent <View, String>>) event -> {
            StringBuilder stringBuilder = new StringBuilder(event.getNewValue());
            if (notation.getNotation(changeNotation
                    .getSelectionModel().getSelectedIndex()).equals("16"))
                stringBuilder = stringBuilder.deleteCharAt(0).deleteCharAt(0);
            ((View)event.getTableView().getItems()
                    .get(event.getTablePosition().getRow()))
                    .setValue((short) (Integer.parseInt(stringBuilder.toString(),
                            Integer.parseInt(notation.getNotation(changeNotation
                                    .getSelectionModel().getSelectedIndex())))));
            event.getTableView().refresh();
        });
        flagN.setCellValueFactory(new PropertyValueFactory<Status, String>("n"));
        flagZ.setCellValueFactory(new PropertyValueFactory<Status, String>("z"));
        flagV.setCellValueFactory(new PropertyValueFactory<Status, String>("v"));
        flagP.setCellValueFactory(new PropertyValueFactory<Status, String>("p"));
        registers.setItems(regViews);
        registers.refresh();
        regStatus.setItems(FXCollections.observableList(Arrays.asList(status)));
        regStatus.refresh();
       }

    public ObservableList<String> createOperand(){
        List<String> lines = new ArrayList <>();
        for (int i = 0; i < COUNT_REGISTERS; i++){
            lines.add((new StringBuilder("R").append(i)).toString());
        }
        return FXCollections.observableArrayList(lines);
    }

    private void initRegisters(){
        for (int i = 0; i < COUNT_REGISTERS; i++){
            View view = new View(register.get(i));
            view.setNotation("16");
            view.setValue((short) 0);
            regViews.add(view);
        }
    }

    public void executeCommands(){
        long result;
        switch (commandChoice.getSelectionModel().getSelectedIndex()){
            case 0: {
                ADD command = loadADD();
                result = command.executeCommand();
                break;
            }
            case 1: {
                SUB command = loadSUB();
                result = command.executeCommand();
                break;
            }

            case 2: {
                AND command = loadAND();
                result = command.executeCommand();
                break;
            }

            case 3: {
                OR command = loadOR();
                result = command.executeCommand();
                break;
            }

            case 4: {
                XOR command = loadXOR();
                result = command.executeCommand();
                break;
            }
            default: {
                System.out.println("Неизвестная команда");
                Command command = null;
                result = 0;
                break;
            }
        }
        checkResult(result);
        regViews.get(secondOperand.getSelectionModel().getSelectedIndex()).setValue((short)result);
        registers.setItems(regViews);
        registers.refresh();
        regStatus.setItems(FXCollections.observableList(Arrays.asList(status)));
        regStatus.refresh();
    }

    private void checkResult(long result) {
        if (result == 0)
            status.setZ(1);
         else
            status.setZ(0);
         if (result >= 0)
             status.setN(0);
         else
             status.setN(1);
         if (result > Short.MAX_VALUE-1)
             status.setV(1);
         else
             status.setV(0);
        if (notation.countUnitsInBinaryNumber(Integer
                .toBinaryString((int)result)) % 2 == 0)
            status.setP(1);
        else
            status.setP(0);
    }

    private ADD loadADD(){
        int num1 = firstOperand.getSelectionModel().getSelectedIndex();
        int num2 = secondOperand.getSelectionModel().getSelectedIndex();
        short reg1 = regViews.get(num1).getShortValue();
        short reg2 = regViews.get(num2).getShortValue();
        return new ADD(reg1, reg2);
    }

    private SUB loadSUB(){
        int num1 = firstOperand.getSelectionModel().getSelectedIndex();
        int num2 = secondOperand.getSelectionModel().getSelectedIndex();
        short reg1 = regViews.get(num1).getShortValue();
        short reg2 = regViews.get(num2).getShortValue();
        return new SUB(reg1, reg2);
    }

    private AND loadAND(){
        int num1 = firstOperand.getSelectionModel().getSelectedIndex();
        int num2 = secondOperand.getSelectionModel().getSelectedIndex();
        short reg1 = regViews.get(num1).getShortValue();
        short reg2 = regViews.get(num2).getShortValue();
        return new AND(reg1, reg2);
    }

    private OR loadOR(){
        int num1 = firstOperand.getSelectionModel().getSelectedIndex();
        int num2 = secondOperand.getSelectionModel().getSelectedIndex();
        short reg1 = regViews.get(num1).getShortValue();
        short reg2 = regViews.get(num2).getShortValue();
        return new OR(reg1, reg2);
    }

    private XOR loadXOR(){
        int num1 = firstOperand.getSelectionModel().getSelectedIndex();
        int num2 = secondOperand.getSelectionModel().getSelectedIndex();
        short reg1 = regViews.get(num1).getShortValue();
        short reg2 = regViews.get(num2).getShortValue();
        return new XOR(reg1, reg2);
    }


    public void changeNotation() {
        for (int i = 0; i < regViews.size(); i++) {
            regViews.get(i).setNotation(notation.getNotation(changeNotation.getSelectionModel().getSelectedIndex()));
        }
        editingCell.changeNotation(notation.getNotation(changeNotation.getSelectionModel().getSelectedIndex()));
        registers.setItems(regViews);
        registers.refresh();
    }
}
