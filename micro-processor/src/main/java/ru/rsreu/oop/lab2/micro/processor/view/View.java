package ru.rsreu.oop.lab2.micro.processor.view;

import ru.rsreu.oop.lab2.micro.processor.functional.Notation;
import ru.rsreu.oop.lab2.micro.processor.memory.Register;

public class View {
    private String notationValue;
    private String name;
    private Register register;
    private Notation notation = new Notation();

    public View(String name){
        this.name = name;
    }

    public short getShortValue(){
        return (short) Integer.parseUnsignedInt(notation.delSymbolsInHex(register.toString()),
                                                Integer.parseInt(notationValue, 10));
    }

    public String getValue(){
        return register.toString();
    }

    public String getName(){
        return name;
    }

    public void setValue(short value) {
        StringBuilder line = new StringBuilder();
        switch (notationValue) {
            case "16": {
                line.append("0x").append(Integer.toHexString(value));
                break;
            }
            case "2": {
                line.append(Integer.toBinaryString(value));
                break;
            }
            default: {
                line = null;
                break;
            }
        }
        this.register = new Register(line.toString(), notationValue);
    }

    @Override
    public String toString(){
        StringBuilder line = new StringBuilder("View[name=");
        line.append(name).append(";value=").append(register.toString()).append(']');
        return line.toString();
    }

    public void setNotation(String notation) {
        if (this.notationValue != null & this.notationValue != notation){
            register.changeNotation(notation);
        }
        this.notationValue = notation;
    }

    public Register getRegister(){
        return register;
    }
}
