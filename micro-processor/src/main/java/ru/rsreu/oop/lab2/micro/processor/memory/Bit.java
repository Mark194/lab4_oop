package ru.rsreu.oop.lab2.micro.processor.memory;

public class Bit extends Data {
    private int value;

    public Bit(int number){
        this.setValue(number);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        if (value == 0 | value == 1)
            this.value = value;
    }

}
