package ru.rsreu.oop.lab2.micro.processor.memory;

public class Status {
    private int n, z, v, p;

    public Status(int n, int p, int v, int z){
        this.n = n;
        this.p = p;
        this.v = v;
        this.z = z;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public int getP() {
        return p;
    }

    public void setP(int p) {
        this.p = p;
    }
}
