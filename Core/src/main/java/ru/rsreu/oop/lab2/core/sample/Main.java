package ru.rsreu.oop.lab2.core.sample;

import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.application.Preloader;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ru.rsreu.oop.lab2.core.preloader.Screen;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(Main.class.getClassLoader().getResource("fxml/sample.fxml"));
        Parent root = loader.load();
        primaryStage.getIcons().add(new Image(Main.class
                .getClassLoader().getResource("img/core.png").toString()));
        primaryStage.setTitle("Лабораторная работа №3");
        primaryStage.setScene(new Scene(root,600, 400));
        primaryStage.show();
    }

    public static void main(String[] args) {
        LauncherImpl.launchApplication(Main.class, Screen.class, args);
    }

    @Override
    public void init() throws Exception{
        this.notifyPreloader(new Preloader.ProgressNotification(0.0));
        Thread.sleep(5000);
        this.notifyPreloader(new Preloader.ProgressNotification(1.0));
    }
}
