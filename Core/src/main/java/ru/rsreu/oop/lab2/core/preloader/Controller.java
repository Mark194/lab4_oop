package ru.rsreu.oop.lab2.core.preloader;

import javafx.fxml.FXML;
import javafx.scene.control.ProgressBar;

public class Controller {
    @FXML
    ProgressBar progressLoad;

    public ProgressBar getProgressLoad(){
        return progressLoad;
    }
}
