package ru.rsreu.oop.lab2.core.sample;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import ru.rsreu.oop.lab2.graphicseditor.form.Main;
import ru.rsreu.oop.lab2.micro.processor.paradigm.Model;

import java.io.IOException;

public class Controller {
    @FXML
    public void loadGraphicsEditor() {
        Main main = new Main();
        try {
            main.start(new Stage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void loadMicroprocessor() {
       Model model = new Model();
        try {
            model.start(new Stage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
